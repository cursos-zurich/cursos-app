<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCurso;
use App\Models\Curso;
use Illuminate\Http\Request;


class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cursos = Curso::orderBy('id', 'desc')->paginate(5);
        return view('cursos.index', compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cursos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCurso $request) //StoreCurso son las reglas de validacion
    {
        /*
        //Validaciones
        $request->validate([
            'name' => 'required | min:3',
            'descripcion' => 'required | max:100',
            'categoria'=> 'required'
        ]);*/
        /*

        $curso = new Curso();
        $curso->name = $request->name;
        $curso->descripcion = $request->descripcion;
        $curso->categoria = $request->categoria;
        */
        //Otra forma de crear el nuevo registro (mejor)
        $curso = Curso::create($request->all());
        

        return redirect()->route('cursos.show', $curso);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function show(Curso $curso)
    {
        
        return view('cursos.show', compact('curso'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function edit(Curso $curso)
    {
        //return $curso->name;
    {
        //return $curso->name;
        return view('cursos.edit', compact('curso'));
    }

    {
        //return $curso->name;
        return view('cursos.edit', compact('curso'));
    }

        return view('cursos.edit', compact('curso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Curso $curso)
    {
        //Validaciones
        $request->validate([
            'name' => 'required | min:3',
            'descripcion' => 'required | max:100',
            'categoria'=> 'required'
        ]);
            /*
        $curso->name = $request->name;
        $curso->descripcion = $request->descripcion;
        $curso->categoria = $request->categoria;

        $curso->save();
        */
        // 2ª forma de actualizar, mejor
        
        $curso->update( $request->all() );
        return redirect()->route('cursos.show', $curso);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Curso  $curso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Curso $curso)
    {
        $curso->delete();
        return redirect()->route('cursos.index');
    }

    public function kk(Request $request){
        $buscar = $request->buscar;
        $cursos = Curso::where("name","like", "%$buscar%")->get();
        return $cursos;
    }
}