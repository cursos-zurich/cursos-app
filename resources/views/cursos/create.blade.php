
@extends('layouts.plantilla')

@section('titulo')
    Crear Curso
@endsection

@section('contenido')

    <h1>Crear Curso</h1>   
    <hr>
    <br>
    <form action="{{ route('cursos.store') }}" method="POST">
        @csrf
        <p>
            <label for="">Titulo:</label>
            <input type="text" name = 'name' value='{{ old('name') }}'>
            @error('name')
                <br><br>
                    *{{ $message }}
                <br>
            @enderror
        </p>
        <p>
            <label for="">Descripcion:</label>
            <textarea name="descripcion" rows="5">{{ old('descripcion') }}</textarea>
            @error('descripcion')
                <br><br>
                    *{{ $message }}
                <br>
            @enderror
        </p>
        <p>
            <label for="">Categoria:</label>
            <input type="text" name = 'categoria' value='{{ old('categoria') }}'>
            @error('categoria')
                <br><br>
                    *{{ $message }}
                <br>
            @enderror
        </p>
        <button type="submit">Guardar</button>
    </form>

@endsection