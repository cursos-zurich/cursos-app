
@extends('layouts.plantilla')

@section('titulo')
    {{ $curso->name }}
@endsection

@section('contenido')

    <p><h1>{{ $curso->name }}</h1> </p> 
    <a href="{{ route('cursos.index') }}">Volver a Cursos</a>
    <br>
    <a href="{{ route('cursos.edit', $curso) }}">Editar Curso</a>
    <br>
    
    <hr>
    <p><strong>Descripcion:</strong> {{ $curso->descripcion }}</p>
    <p><strong>Categoria:</strong> {{ $curso->categoria }}</p>
    <br>
    <form action="{{ route('cursos.destroy', $curso) }}" method='POST'>
        @csrf
        @method('delete')
        <button type="submit">Eliminar</button>
    </form>
    
@endsection