
@extends('layouts.plantilla')

@section('titulo')
    Cursos
@endsection

@section('contenido')

    <h1>Cursos disponibles</h1>   
    <form action= '{{ route('cursos.buscar') }}'>
        <label >Buscar</label>
        <input type="text" name="buscar" >
        <button type="submit">Buscar</button>
    </form>
    <a href={{ route('cursos.create') }}>Crear Curso</a>
    <hr>
    <ul>
        @foreach ($cursos as $curso)
            <li>
                <a href="{{ route('cursos.show', $curso) }}">{{ $curso->name }}</a>                
            </li>
            
            
        @endforeach 
    </ul>
    {{ $cursos->links() }}

@endsection