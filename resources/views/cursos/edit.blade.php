
@extends('layouts.plantilla')

@section('titulo')
    Editar Curso: {{ $curso->name }}
@endsection

@section('contenido')

    <h1>Editar Curso</h1>   
    <hr>
    <br>
    <form action="{{ route('cursos.update', $curso) }}" method="POST">
        @csrf
        @method('PUT')
        <p>
            <label>Titulo:</label>
            <input type="text" name = 'name'  value='{{ old('name', $curso->name) }}'>
            @error('name')
                <br><br>
                    *{{ $message }}
                <br>
            @enderror
        </p>
        <p>
            <label>Descripcion:</label>
            <textarea name="descripcion" rows="5">{{ old('descripcion', $curso->descripcion) }}</textarea>
            @error('descripcion')
                <br><br>
                    *{{ $message }}
                <br>
            @enderror
        </p>
        <p>
            <label>Categoria:</label>
            <input type="text" name = 'categoria'  value='{{ old('categoria', $curso->categoria) }}'>
            @error('categoria')
                <br><br>
                    *{{ $message }}
                <br>
            @enderror
        </p>
        <button type="submit">Actualizar</button>
    </form>

@endsection